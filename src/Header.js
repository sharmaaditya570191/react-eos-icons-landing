import React from 'react'

const Header = () => {
    return (
        <nav class="navbar navbar-expand-lg top-gradient navbar-dark nav-border fixed-top forward-index">
        <div class="navbar-brand" href="#"><img class="d-inline-block align-top mt-lg-2" src="assets/images/landing/logo.png" width="86" height="35" alt=""></div>
        <a class="navbar-brand" href="/">EOS Icons</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav text-white ml-auto">
            <li class="nav-item"><a class="nav-link current" href="/">Home</a></li>
            <li class="nav-item"><a class="nav-link current" href="cheatsheet.html">Cheatsheet</a></li>
            <li class="nav-item"><a class="nav-link current" href="icons-picker.html">Customize it</a></li>
            <li class="nav-item"><a class="nav-link current" href="docs.html">Docs</a></li>
        </ul>
        </div>
        </nav>
    )
}